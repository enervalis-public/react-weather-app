# The Weather Station
## The Concept

For this assignment, you will build a responsive web application using your favorite framework(s). The app shows the weatherforecast for the user's current location. It show the forecast for today, but also for the next 5 days.

## The Assignment

At the end of this exercise, you should demonstrate a clean, optimized version of the application, ready to run on a server. Therefore, it is important to take into account loading times, ES standards and support for multiple browsers. **You must use React.js** for this assignment. Everything else is up to you to decide.

The application consists out of a single screen, showing the weather forecast of your current location. The forecast shows a description and an icon depicting the current status, the current temperature as well as the minimum and maximum temperature for today. All temperatures should be shown in degrees celcius.

This screen also shows the five day forecast, with the name of the day, an icon summarizing the weather conditions and the expected minimum and maximum temperature for the day.

In designs folder, you will find some screenshots of the app you need to build. Notice how the layout is different depending on the screen size of the device used to visit the site!

If you're not connected to the internet, or if there is a problem reaching the API, the user should be notified of this.

Apart from javascript, your application will need some styling. Since the app isn't very complex, we don't think a CSS framework is required. The colors used in the screenshots are blue (#4A90E2); yellow (#F8E71C); and white. 

## The data

To get to the weather data, please use (https://openweathermap.org/")[https://openweathermap.org/].

For today's forecast, use (https://openweathermap.org/current")[https://openweathermap.org/current], and for the five day forecast, use (https://openweathermap.org/forecast5")[https://openweathermap.org/forecast5]

Use the info provided at (https://openweathermap.org/weather-conditions)[https://openweathermap.org/weather-conditions] to make the app visually attractive. You can load the weather icons from the OpenWeatherMap site, or use the ones provided in the images folder.

Use the following API key: openweathermap-key: e5f670fdf8c2ffde3c546ffe925fec22

**BE CAREFUL**
The API key that we use is a free key. The free key is limited to 60calls/minute. If you exceed this, the API key will be blocked for some amount of time. Write your code carefully and always test first before running it with the actual API.
